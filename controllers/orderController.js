const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User')


// Create order
module.exports.order = async(data) => {
let newOrder = new Order({
	userId: data.userId,
	})
if(!data.isAdmin){
	let isProductUpdated = await Product.findById(data.order.productId).then(product =>{
		
		newOrder.products.push({productId: data.order.productId, quantity: data.order.quantity, productName: product.name})

		newOrder.totalAmount = product.price * data.order.quantity
		return newOrder.save().then((order, err)=>{
			if(err){
				return false;
			}else{
				return true;
			}
		})
	})
	if(isProductUpdated){
		return true;
	} else {
		return false;
	}
}
let message = Promise.resolve('User must not be ADMIN to create order');
	return message.then((value)=>{
		return {value};
	})
}


