const Product = require("../models/Product");

// Add product as an Admin
module.exports.createProduct = (data) => {

	if (data.isAdmin) {
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value) => {
		return {value};
	})
};


// Retrieve Active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};

// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// Update a product
module.exports.updateProduct = (reqParams,data) => {
	if (data.isAdmin) {
		let updateProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		};
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve("You must be ADMIN to access this!");
	return message.then((value) => {
		return {value};
	})
};

// Archive a Product
module.exports.archiveProduct = (reqParams, data) => {
	if (data.isAdmin) {
		let archivedProduct = {
			isActive: false
		};

		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, err) => {
			if(err) {
				return false;
			} else {
				return true;
			}
		})
	}
	let message = Promise.resolve("You must be ADMIN to access this!");
	return message.then((value) => {
		return {value};
	})
};


	
