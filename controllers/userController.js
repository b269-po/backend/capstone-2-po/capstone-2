const User = require("../models/User");
const Product = require("../models/Product");
const mongoose = require("mongoose");

const bcrypt = require("bcrypt");

const auth = require("../auth");

// Register User
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		userName: reqBody.userName,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

// User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

// Retrieve user details
module.exports.getUser = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// Set User as an Admin
module.exports.setUser = (data)=>{
	if(data.isAdmin){
		let updatedUser = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(data.userId, updatedUser).then((user,error)=>{
				if(error){
					return false
				} else {
					let message = `${user.userName} is now an Admin.`
					return message;
				}
			})
	}
	let message = Promise.resolve('Only Admins can access this!');
	return message.then(result =>{
		return {result}
	})
}