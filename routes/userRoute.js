const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");

// User register
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Login Auth
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user
router.post("/user", auth.verify, (req, res) => {

		const userData = auth.decode(req.headers.authorization);

	userController.getUser({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// Update user as an Admin
router.put('/set-user',auth.verify,(req,res)=>{
	let data ={
		userId: req.body.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setUser(data).then(result =>res.send(result));
})

module.exports = router;