const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");

// Create product
router.post('/create', auth.verify, (req,res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(data).then(result =>res.send(result));
})

// Retrieve all Active products
router.get('/active', (req,res)=>{
	productController.getActiveProducts().then(result => res.send(result));
})

// Retrieve a Single product
router.get('/:productId', auth.verify, (req,res)=>{
	productController.getProduct(req.params).then(result => res.send(result));
})

// Update Product info
router.put('/:productId/update', auth.verify, (req,res)=>{
	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(req.params,data).then(result => res.send(result));
})

// Archive a Product
router.patch('/:productId/archive', auth.verify, (req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params, data).then(result =>{
		res.send(result)
	})
})

module.exports = router;