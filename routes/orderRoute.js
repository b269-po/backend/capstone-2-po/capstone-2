const orderController = require('../controllers/orderController');
const express = require('express');
const router = express.Router();
const auth = require('../auth');


// create order
router.post('/checkout', auth.verify, (req,res) => {
	let data = {
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	orderController.order(data).then(result =>res.send(result));
})


module.exports = router;