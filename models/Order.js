const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: true
	},
	products: [{
		productId: {
			type: String,
			required: true
		},
		quantity: {
			type: Number,
			required: true
		},
		productName: {
			type: String,
			required: true
		}
	}],
	totalAmount:{
		type: Number,
		required: true
	},
	purchasedOn: {
		type: Date,
	default: new Date()
	}
});

module.exports = new mongoose.model('Order', orderSchema)

