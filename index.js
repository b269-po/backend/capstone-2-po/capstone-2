const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoute = require("./routes/userRoute");

const productRoute = require("./routes/productRoute");

const orderRoute = require('./routes/orderRoute');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.use("/users", userRoute);
app.use("/products", productRoute);
app.use('/orders', orderRoute);

// Database connection
mongoose.connect("mongodb+srv://jicksonpo:admin123@zuitt-bootcamp.r4vxd0d.mongodb.net/eCommerceAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});
mongoose.connection.once("open", () => console.log("Now connected to cloud database!"));


app.listen(process.env.PORT || 3000, () => console.log(`Now connected to port ${process.env.PORT || 3000}`));